# README #

This project builds a Visual Studio extension (VSIX) containing code snippets commonly used in Beckman projects.

## Use

The C# snippets are mostly standalone code generators. Simply type the snippet shortcut and hit `Tab` twice to autogenerate the code defined in the snippet.

The HTML element snippets are designed to surround a selection, as when converting text pasted from a word document. Select the text you wish to wrap, press `ctrl+k, ctrl+s` to open the surround dialog, navigate to the appropriate snippet, and press `Tab` to wrap the selection in an HTML element.
 
## Common Installation

1. In visual studio, open Tools > Options.
2. Click Extensions and Updates, and scroll to the Additional Extension Galleries section.
3. Click Add. Change Name to "Beckman Private Gallery", and URL to `file:///<X>:/<PathToGallery>/VisualStudioExtensions/atom.xml`. Click Apply.
4. Open Tools > Extensions and Updates.
5. Expand the Online galleries. You should see a Beckman gallery with an update to the snippets extension.

## Manual Installation

1. Clone this project and build in Visual Studio.
2. In File Explorer, navigate to <SolutionDirectory>/Beckman.Snippets/bin/Release/.
3. Double-click the Beckman.Snippets.vsix file and follow the installation instructions.

## Updating the Extension

1. Add new snippet.
2. Update version number in source.extension.vsixmanifest.
3. Note change in ReleaseNotes.txt
4. Build project using Release configuration.
5. Copy Beckman.Snippets.vsix file from bin/Release/ to gallery directory.
6. Update atom.xml. This will have to be done by hand until we have a real distribution mechanism.

## Known Issues

ReSharper uses its own template language and overrides Visual Studio's Intellisense defaults. If using ReSharper, either live without intellisense prompts for these snippets, rewrite the snippets as ReSharper live templates, or tell ReSharper to use Visual Studio settings for snippet Intellisense.

## Updating the Gallery Feed

VSIX templates are distributed via Visual Studio Gallery feeds, not NuGet package. See [Shipping Visual Studio Extensions](https://msdn.microsoft.com/en-us/library/ff363239.aspx) and [Shipping Visual Studio Snippets in an Extension](http://madskristensen.net/post/shipping-visual-studio-snippets-in-an-extension) for more information.

0. Add or edit your snippet.
0. Update [ReleaseNotes.txt](Beckman.Snippets/ReleaseNotes.txt).
0. In Visual Studio's Solution Explorer, open [source.extension.vsixmanifest](Beckman.Snippets/source.extension.vsixmanifest) and update the version number.
0. Merge to master and tag the merge commit with the new version number.
0. Build your solution.
0. Copy the new `.vsix` file from `Beckman.Snippets/bin/Debug/` to the private nuget feed and update the atom.xml file for the feed.
